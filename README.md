SKY Mobile Test
===============================

The basic MVC/MVP is organized as described below:

* network - basic network communication and data parses
* model - data model representation
* controller - responsible to business logic and decoupled from UI layer
* ui - responsible to preseting data to user and decoupled from business layer
* androidTest - basic test example interacting with UI components

Developed by
============

[José Torres](https://github.com/torrescalazans)
