package com.sky.mobile;

/**
 * Created by torres on 5/31/17.
 */

public final class Constants {

    // URL to get JSON catalog
    public static String CATALOG_URL = "https://sky-exercise.herokuapp.com/api/movies/";

}
