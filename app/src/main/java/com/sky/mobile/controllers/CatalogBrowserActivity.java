package com.sky.mobile.controllers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.sky.mobile.Constants;
import com.sky.mobile.model.CatalogItem;
import com.sky.mobile.network.communication.HttpHandler;
import com.sky.mobile.network.communication.NetworkCallback;
import com.sky.mobile.ui.screens.catalogBrowser.CatalogBrowserView;

import java.util.List;

/**
 * Created by torres on 5/31/17.
 */

public class CatalogBrowserActivity extends AbstractActivity implements CatalogBrowserView.OnItemClickListener, NetworkCallback<List<CatalogItem>> {

    private static final String LOG_TAG = CatalogBrowserActivity.class.getSimpleName();

    private HttpHandler mHttpHandler;
    private CatalogBrowserView mCatalogBrowserView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCatalogBrowserView = new CatalogBrowserView(LayoutInflater.from(this));
        mCatalogBrowserView.registerListener(this);
        setContentView(mCatalogBrowserView.getView());

        mHttpHandler = new HttpHandler(Constants.CATALOG_URL);
        mHttpHandler.setCallback(this);
        mHttpHandler.execute();
    }

    /**
     * Indicates that the callback handler needs to update its appearance or information based on
     * the result of the task. Expected to be called from the main thread.
     *
     * @param catalogData
     */
    @Override
    public void updateData(List<CatalogItem> catalogData) {
        Log.d(LOG_TAG, "updateData - catalogData: " + catalogData);

        mCatalogBrowserView.updateData(catalogData);
    }

    @Override
    public void onItemLongClick(View view, int position) {
        Log.d(LOG_TAG, "onItemLongClick - position: " + position);
        // TODO start catalog item details screen

    }
}
