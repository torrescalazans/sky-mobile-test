package com.sky.mobile.network.communication;

/**
 * Created by torres on 5/31/17.
 */
public interface NetworkCallback<T> {

    /**
     * Indicates that the callback handler needs to update its appearance or information based on
     * the result of the task. Expected to be called from the main thread.
     */
    void updateData(T result);
}
