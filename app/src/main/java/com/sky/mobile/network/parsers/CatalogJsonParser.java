package com.sky.mobile.network.parsers;

import android.util.Log;

import com.sky.mobile.model.CatalogItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by torres on 5/31/17.
 */

public class CatalogJsonParser {

    private final String LOG_TAG = CatalogJsonParser.class.getSimpleName();

    private final String CATALOG_ITEM_ID = "id";
    private final String CATALOG_ITEM_TITLE = "title";
    private final String CATALOG_ITEM_OVERVIEW = "overview";
    private final String CATALOG_ITEM_DURATION = "duration";
    private final String CATALOG_ITEM_RELEASE_YEAR = "release_year";
    private final String CATALOG_ITEM_COVER_URL = "cover_url";
    private final String CATALOG_ITEM_BACKDROPS_URL = "backdrops_url";

    private String mCatalogData;
    private List<CatalogItem> mCatalogItems;

    public CatalogJsonParser(String catalogData) {
        mCatalogData = catalogData;
        mCatalogItems = new ArrayList<CatalogItem>();
    }

    public List<CatalogItem> parse() {
        Log.d(LOG_TAG, "parse: " + mCatalogData);

        try {
            JSONArray jsonArray = new JSONArray(mCatalogData);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                CatalogItem catalogItem = new CatalogItem(
                    jsonObject.getString(CATALOG_ITEM_ID),
                    jsonObject.getString(CATALOG_ITEM_TITLE),
                    jsonObject.getString(CATALOG_ITEM_OVERVIEW),
                    jsonObject.getString(CATALOG_ITEM_DURATION),
                    jsonObject.getString(CATALOG_ITEM_RELEASE_YEAR),
                    jsonObject.getString(CATALOG_ITEM_COVER_URL),
                    jsonObject.getString(CATALOG_ITEM_BACKDROPS_URL)
                );

                mCatalogItems.add(catalogItem);
            }

            for(CatalogItem catalogItem: mCatalogItems) {
                Log.d(LOG_TAG, catalogItem.toString());
            }

        } catch(JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG,"Error processing catalog JSON data");
        }

        return mCatalogItems;
    }
}
