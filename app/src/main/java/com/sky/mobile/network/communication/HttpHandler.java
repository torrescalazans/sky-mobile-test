package com.sky.mobile.network.communication;

import android.os.AsyncTask;
import android.util.Log;

import com.sky.mobile.model.CatalogItem;
import com.sky.mobile.network.parsers.CatalogJsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by torres on 5/31/17.
 */

public class HttpHandler extends AsyncTask<String, Void, String> {

    private final String LOG_TAG = HttpHandler.class.getSimpleName();

    private String mUrl;
    private String mCatalogData;

    private NetworkCallback<List<CatalogItem>> mNetworkCallback;


    public HttpHandler(String url) {
        mUrl = url;
    }

    public String getCatalogData() {
        return mCatalogData;
    }

    public void setCallback(NetworkCallback<List<CatalogItem>> callback) {
        mNetworkCallback = callback;
    }

    public void execute() {
        this.execute(mUrl);
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected String doInBackground(String... params) {
        Log.d(LOG_TAG, "doInBackground - params: " + params[0]);

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        if (params == null)
            return null;

        try {
            URL url = new URL(params[0]);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream == null) {
                return null;
            }

            StringBuffer buffer = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            return buffer.toString();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error downloading catalog", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch(IOException e) {
                    Log.e(LOG_TAG,"Error closing stream", e);
                }
            }
        }
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param catalogData The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(String catalogData) {
        super.onPostExecute(catalogData);

        Log.d(LOG_TAG, "onPostExecute - catalogData: " + catalogData);
        Log.d(LOG_TAG, "onPostExecute - mNetworkCallback: " + mNetworkCallback);

        if (catalogData != null && mNetworkCallback != null) {
            mCatalogData = catalogData;
            CatalogJsonParser catalogJsonParser = new CatalogJsonParser(mCatalogData);
            mNetworkCallback.updateData(catalogJsonParser.parse());
        }
    }
}
