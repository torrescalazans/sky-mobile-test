package com.sky.mobile.ui.screens.catalogBrowser;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;

import com.sky.android.skymobile.R;
import com.sky.mobile.model.CatalogItem;
import com.sky.mobile.ui.core.AbstractView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by torres on 5/31/17.
 */

public class CatalogBrowserView extends AbstractView {

    private static final String LOG_TAG = CatalogBrowserView.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private GestureDetector mGestureDetector;
    private CatalogRecyclerViewAdapter mCatalogRecyclerViewAdapter;
    private static final int numberOfColumns = 2;

    public CatalogBrowserView(LayoutInflater inflater) {
        setView(inflater.inflate(R.layout.content_catalog_browser, null, false));

        mRecyclerView = findViewById(R.id.catalog_browser_recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));

        int spanCount = 2; // 2 columns
        int spacing = 20; // 20px
        boolean includeEdge = true;
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

        mCatalogRecyclerViewAdapter = new CatalogRecyclerViewAdapter(getContext(),
                new ArrayList<CatalogItem>());

        mRecyclerView.setAdapter(mCatalogRecyclerViewAdapter);

//        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
//            @Override
//            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e) {
//                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
//
//                for (OnItemClickListener listener : getListeners()) {
//                    listener.onItemLongClick(childView, recyclerView.getChildLayoutPosition(childView));
//                }
//
//                return false;
//            }
//
//            @Override
//            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//
//            }
//
//            @Override
//            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//            }
//        });
    }

    /**
     * Update view data to presenting to the user.<br>
     *
     * @param data
     */
    @Override
    public void updateData(Object data) {
        mCatalogRecyclerViewAdapter.updateData((List<CatalogItem>)data);
    }

    private class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
