package com.sky.mobile.ui.core;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.View;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by torres on 5/31/17.
 */

public abstract class AbstractView implements ObservableView {

    /**
     * Root view
     */
    private View mView;

    private Set<OnItemClickListener> mListeners =
            Collections.newSetFromMap(new ConcurrentHashMap<OnItemClickListener, Boolean>(1));

    /**
     * Listener that will notify of any long click input events performed on MVC view
     *
     * @param listener
     */
    @Override
    public void registerListener(OnItemClickListener listener) {
        if (listener != null) mListeners.add(listener);
    }

    /**
     * Unregister
     *
     * @param listener
     */
    @Override
    public void unregisterListener(OnItemClickListener listener) {
        mListeners.remove(listener);
    }

    /**
     * Root Android View used to presenting data to the user.<br>
     * <p>
     * The returned Android View might be used by Controller in order to query/update
     * properties of the root Android View or any of its child View's.
     *
     * @return root Android View of MVC View
     */
    @Override
    public View getView() {
        return mView;
    }

    protected void setView(@NonNull View view) {
        mView = view;
    }

    protected <T extends View> T findViewById(@IdRes int id) {
        return (T) mView.findViewById(id);
    }

    protected Context getContext() {
        return mView.getContext();
    }

    /**
     * Get a reference to the (thread safe) set containing all the registered listeners. Note that
     * the returned reference is a reference to the set itself, not to its copy.
     */
    protected Set<OnItemClickListener> getListeners() {
        return mListeners;
    }
}
