package com.sky.mobile.ui.screens.catalogBrowser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sky.android.skymobile.R;
import com.sky.mobile.model.CatalogItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by torres on 5/31/17.
 */

public class CatalogRecyclerViewAdapter extends RecyclerView.Adapter<CatalogItemViewHolder> {

    private static final String LOG_TAG = CatalogRecyclerViewAdapter.class.getSimpleName();

    private Context mContext;
    private List<CatalogItem> mCatalogItems;

    public CatalogRecyclerViewAdapter(Context context, List<CatalogItem> catalogItems) {
        mContext = context;
        mCatalogItems = catalogItems;
    }

    @Override
    public CatalogItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.catalog_item, null);
        CatalogItemViewHolder catalogItemViewHolder = new CatalogItemViewHolder(view);

        return catalogItemViewHolder;
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle efficient partial bind.
     *
     * @param catalogItemViewHolder The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(CatalogItemViewHolder catalogItemViewHolder, int position) {
        CatalogItem catalogItem = mCatalogItems.get(position);

        Picasso.with(mContext).load(catalogItem.getCoverUrl())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(catalogItemViewHolder.mThumbnail);
        catalogItemViewHolder.mTitle.setText(catalogItem.getTitle());
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return (null != mCatalogItems ? mCatalogItems.size() : 0);
    }

    public void updateData(List<CatalogItem> catalogItems) {
        mCatalogItems = catalogItems;
        notifyDataSetChanged();
    }
}
