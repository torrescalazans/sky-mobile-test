package com.sky.mobile.ui.core;

import android.view.View;

/**
 * Created by torres on 5/31/17.
 */

public interface BaseView {

    /**
     * Root Android View used to presenting data to the user.<br>
     *
     * The returned Android View might be used by Controller in order to query/update
     * properties of the root Android View or any of its child View's.
     *
     * @return root Android View of MVC View
     */
    View getView();

    /**
     * Update view data to presenting to the user.<br>
     *
     * @param data
     */
    void updateData(Object data);
}
