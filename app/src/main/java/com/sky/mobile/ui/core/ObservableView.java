package com.sky.mobile.ui.core;

/**
 * Created by torres on 5/31/17.
 */

import android.view.View;
/**
 * Interface to notify MVC controllers of input events
 */
public interface ObservableView extends BaseView {

    interface OnItemClickListener
    {
        public void onItemLongClick(View view, int position);
    }

    /**
     * Listener that will notify of any long click input events performed on MVC view
     */
    void registerListener(OnItemClickListener listener);

    /**
     * Unregister
     */
    void unregisterListener(OnItemClickListener listener);
}
