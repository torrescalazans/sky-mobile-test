package com.sky.mobile.ui.screens.catalogBrowser;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sky.android.skymobile.R;

/**
 * Created by torres on 5/31/17.
 */

public class CatalogItemViewHolder extends RecyclerView.ViewHolder {

    private static final String LOG_TAG = CatalogItemViewHolder.class.getSimpleName();

    protected ImageView mThumbnail;
    protected TextView mTitle;

    public CatalogItemViewHolder(View itemView) {
        super(itemView);

        this.mThumbnail = (ImageView) itemView.findViewById(R.id.catalog_item_thumbnail);
        this.mTitle = (TextView) itemView.findViewById(R.id.catalog_item_title);
    }
}
