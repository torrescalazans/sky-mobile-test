package com.sky.mobile.model;

import java.io.Serializable;

/**
 * Created by torres on 5/31/17.
 */

public class CatalogItem implements Serializable {

    private static final long versionUID = 1L;

    private String mId;
    private String mTitle;
    private String mOverview;
    private String mDuration;
    private String mReleaseYear;
    private String mCoverUrl;
    private String mBackdropsUrl;

    public CatalogItem(String id, String title, String overview, String duration, String releaseYear, String coverUrl, String backdropsUrl) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mDuration = duration;
        mReleaseYear = releaseYear;
        mCoverUrl = coverUrl;
        mBackdropsUrl = backdropsUrl;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getOverview() {
        return mOverview;
    }

    public String getDuration() {
        return mDuration;
    }

    public String getReleaseYear() {
        return mReleaseYear;
    }

    public String getCoverUrl() {
        return mCoverUrl;
    }

    public String getBackdropsUrl() {
        return mBackdropsUrl;
    }

    @Override
    public String toString() {
        return "CatalogItem{" +
                "mId='" + mId + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", mOverview='" + mOverview + '\'' +
                ", mDuration='" + mDuration + '\'' +
                ", mReleaseYear='" + mReleaseYear + '\'' +
                ", mCoverUrl='" + mCoverUrl + '\'' +
                ", mBackdropsUrl='" + mBackdropsUrl + '\'' +
                '}';
    }
}
