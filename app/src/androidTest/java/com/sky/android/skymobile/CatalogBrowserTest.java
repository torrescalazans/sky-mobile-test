package com.sky.android.skymobile;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.sky.mobile.controllers.CatalogBrowserActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class CatalogBrowserTest {

    @Rule
    public ActivityTestRule<CatalogBrowserActivity> mActivityRule = new ActivityTestRule<>(
            CatalogBrowserActivity.class);

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.sky.skymobile", appContext.getPackageName());
    }

    @Test
    public void checkCatalogItemsQuantity() {
        onView(withId(R.id.catalog_browser_recycler_view)).check(new RecyclerViewItemCountAssertion(10));
    }
}
